using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Formatting;
using Microsoft.CodeAnalysis.Editing;
using Microsoft.CSharp;

using System;
using System.Collections.Generic;

namespace Gir.Generation
{
    using Gir.Analysis;

    class MethodBuilder : BuilderBase
    {
        private SyntaxNode returnType;
        private string delegateName;
        private Method _method;
        private List<SyntaxNode> paramArray;

        public MethodBuilder(SyntaxGenerator gen, Method methodInfo) : base(gen)
        {
            _method = methodInfo;
        }

        public IEnumerable<SyntaxNode> Build()
        {
            delegateName = "d_" + _method.CIdentifier;
            returnType = GetReturnType();
            paramArray = ConstructParameters();

            // Return multiple statements
            return new[] {
                BuildDelegate(),
                BuildStaticField(),
                BuildMethodBody()
            };
        }

        private List<SyntaxNode> ConstructParameters()
        {
            // Construct parameter list
            List<SyntaxNode> array = new List<SyntaxNode>();
            /*foreach (KeyValuePair<string, TypeInfo> parameter in _method.Parameters)
            {
                string paramName = parameter.Key;
                string paramType = (parameter.Value as TypeInfo).NativeType;

                SyntaxNode paramDecl = Syntax.ParameterDeclaration(paramName, SyntaxFactory.ParseTypeName(paramType));
                paramArray.Add(paramDecl);
            }*/

            return array;
        }

        private SyntaxNode GetReturnType()
        {
            if (_method.ReturnValue != null)
            {
                if (_method.ReturnValue.IsArray)
                    return MarshalAsToken(_method.ReturnValue.Array.Name);
                else
                    return MarshalAsToken(_method.ReturnValue.Type.Name);
            }

            // We cannot handle this case
            throw new NotImplementedException();
        }

        private SyntaxNode BuildMethodBody()
        {

            return Syntax.MethodDeclaration(
                name: Utils.SnakeToPascalCase(_method.Name),
                returnType: returnType,
                parameters: paramArray,
                statements: new[] {
                    CallNativeAndReturn()
                }
            );

        }

        private SyntaxNode CallNativeAndReturn()
        {
            SyntaxNode nativeCall = Syntax.InvocationExpression(
                Syntax.IdentifierName(_method.CIdentifier),
                paramArray
            );

            // Return type is not void so we will
            // need to marshal/cast the return value
            if ((returnType as TypeSyntax).ToString() != "void")
            {
                // TODO: Casting/Marshalling
                return Syntax.ReturnStatement(
                    Syntax.CastExpression(
                        returnType,
                        nativeCall
                    )
                );
            }

            // No return type, so do not return a value
            return nativeCall;
        }

        private SyntaxNode BuildStaticField()
        {

            return Syntax.FieldDeclaration(
                name: _method.CIdentifier,
                type: SyntaxFactory.ParseTypeName(delegateName),
                modifiers: DeclarationModifiers.Static,
                initializer: Syntax.InvocationExpression(
                    Syntax.WithTypeArguments(
                        Syntax.MemberAccessExpression(
                            Syntax.IdentifierName("FuncLoader"),
                            Syntax.IdentifierName("LoadFunction")
                        ),
                        Syntax.IdentifierName(delegateName)
                    ),
                    Syntax.Argument(
                        "procaddress",
                        RefKind.None,
                        Syntax.InvocationExpression(
                            Syntax.MemberAccessExpression(
                                Syntax.IdentifierName("FuncLoader"),
                                Syntax.IdentifierName("GetProcAddress")
                            ),
                            Syntax.Argument(
                                "library",
                                RefKind.None,
                                Syntax.InvocationExpression(
                                    Syntax.MemberAccessExpression(
                                        Syntax.IdentifierName("GLibrary"),
                                        Syntax.IdentifierName("Load")
                                    ),
                                    Syntax.Argument(
                                        "library",
                                        RefKind.None,
                                        // TODO: WE NEED TO DETERMINE THIS BASED ON THE LIBRARY ! IMPORTANT
                                        Syntax.MemberAccessExpression(
                                            Syntax.IdentifierName("Library"),
                                            Syntax.IdentifierName("Atk")
                                        )
                                    )
                                )
                            ),
                            Syntax.Argument(
                                "function",
                                RefKind.None,
                                Syntax.LiteralExpression(_method.CIdentifier)
                            )
                        )
                    )
                )
            );

        }

        public SyntaxNode BuildDelegate()
        {

            // Delegate with attributes
            return Syntax.AddAttributes(

                // Creates the function signature`
                Syntax.DelegateDeclaration(
                    name: delegateName,
                    parameters: paramArray,
                    returnType: returnType
                ),
                new[] {
                    // Attribute: Marks this delegate as an unmanaged pointer into native code
                    Syntax.Attribute(
                        Syntax.IdentifierName("UnmanagedFunctionPointer"),
                        new[] {
                            Syntax.MemberAccessExpression(
                                Syntax.IdentifierName("CallingConvention"),
                                Syntax.IdentifierName("Cdecl")
                            )
                        }
                    )
                }
            );

        }

    }
}