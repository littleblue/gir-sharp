using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Formatting;
using Microsoft.CodeAnalysis.Editing;
using Microsoft.CSharp;

using System;
using System.Collections.Generic;

namespace Gir.Generation
{
    using Gir;
    using Gir.Analysis;
    using Gir.Marshal;

    class EnumBuilder : BuilderBase
    {
        private Enumeration _enum;
        public EnumBuilder(SyntaxGenerator gen, Enumeration enumInfo) : base(gen)
        {
            _enum = enumInfo;
        }

        public IEnumerable<SyntaxNode> Build()
        {
            return new[] {
                // Enum Declaration with attributes
                Syntax.AddAttributes(
                    Syntax.EnumDeclaration(
                        name: _enum.Name,
                        members: GetEnumMembers()
                    ),
                    // Attribute: Marks this enum with a specific GType
                    Syntax.Attribute(
                        Syntax.IdentifierName("GLib.GType"),
                        new[] {
                            Syntax.TypeOfExpression(
                                SyntaxFactory.ParseTypeName(_enum.Name + "GType")
                            )
                        }
                    )
                )
            };
        }

        private IEnumerable<SyntaxNode> GetEnumMembers()
        {
            foreach (Member member in _enum.Members)
            {
                yield return Syntax.EnumMember(
                    Utils.SnakeToPascalCase(member.Name),
                    Syntax.LiteralExpression(Int32.Parse(member.Value))
                );
            }
        }
    }
}