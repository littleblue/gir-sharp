using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Formatting;
using Microsoft.CodeAnalysis.Editing;

using System.Text;
using System.IO;
using System.Collections.Generic;
using System;


namespace Gir.Generation
{
    using Gir.Analysis;
    class NamespaceBuilder
    {
        private string workingDir;
        private AdhocWorkspace workspace;
        private string dirPath;
        private SyntaxGenerator syntaxGenerator;
        private Namespace _namespace;

        public NamespaceBuilder(Namespace namespaceInfo, string directory)
        {
            _namespace = namespaceInfo;
            workingDir = directory;
            dirPath = EnsureCreateDirectory(_namespace);
            workspace = new AdhocWorkspace();
            syntaxGenerator = SyntaxGenerator.GetGenerator(workspace, LanguageNames.CSharp);
        }

        public void Build()
        {
            GenerateClasses();
            GenerateEnums();
        }

        private void GenerateClasses()
        {
            // Generate Classes            
            foreach (Class classInfo in _namespace.Classes)
            {
                // Write class to file
                ClassBuilder builder = new ClassBuilder(syntaxGenerator, classInfo);
                WriteSyntaxNodesToFile(builder.Build(), classInfo.Name + ".cs");
            }
        }

        private void GenerateEnums()
        {
            // Generate Enums
            foreach (Enumeration enumInfo in _namespace.Enumerations)
            {
                // Write enum to file
                EnumBuilder builder = new EnumBuilder(syntaxGenerator, enumInfo);
                WriteSyntaxNodesToFile(builder.Build(), enumInfo.Name + ".cs");
            }
        }

        private void WriteSyntaxNodesToFile(IEnumerable<SyntaxNode> nodes, string filename)
        {
            // Create a Compilation Unit
            CompilationUnitSyntax compilationUnit = (CompilationUnitSyntax)syntaxGenerator.CompilationUnit(
                SyntaxFactory.UsingDirective(SyntaxFactory.IdentifierName("System")),
                SyntaxFactory.UsingDirective(SyntaxFactory.IdentifierName("System.Runtime.InteropServices")),
                syntaxGenerator.NamespaceDeclaration(
                    syntaxGenerator.IdentifierName(_namespace.Name),
                    nodes
                )
            ).NormalizeWhitespace();

            // Output to file
            SyntaxNode formattedNode = Formatter.Format(compilationUnit, workspace);
            string path = Path.Combine(dirPath, filename);
            using (StreamWriter writer = new StreamWriter(path)) {
                formattedNode.WriteTo(writer);
            }
        }

        private string EnsureCreateDirectory(Namespace _namespace)
        {
            // Avoid Collisions
            string dirPath = Path.Combine(workingDir, _namespace.Name);
            if (Directory.Exists(dirPath))
            {
                int i = 1;
                while (Directory.Exists(dirPath + i.ToString()))
                {
                    i += 1;
                }
                dirPath += i.ToString("D2");
            }

            // Create the directory
            Directory.CreateDirectory(dirPath);

            return dirPath;
        }
    }
}