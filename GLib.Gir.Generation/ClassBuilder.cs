using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Formatting;
using Microsoft.CodeAnalysis.Editing;
using Microsoft.CSharp;

using System.Collections.Generic;

namespace Gir.Generation
{
    using Gir.Analysis;

    class ClassBuilder : BuilderBase
    {
        private Class _class;

        public ClassBuilder(SyntaxGenerator gen, Class classInfo) : base(gen)
        {
            _class = classInfo;
        }

        // Returns a SyntaxNode representing a class
        // It is the caller's responsibility to subdivide this
        // into CompilationUnit(s)
        public IEnumerable<SyntaxNode> Build()
        {
            SyntaxNode parentType = null;
            if (_class.Parent != null)
                parentType = MarshalAsToken(_class.Parent);

            return new[] {
                Syntax.AddBaseType(
                    Syntax.ClassDeclaration(
                        name: _class.Name,
                        members: BuildMembers(),
                        modifiers: DeclarationModifiers.Partial
                    ),
                    parentType
                )
            };
        }

        private IEnumerable<SyntaxNode> BuildMembers()
        {
            List<SyntaxNode> memberNodes = new List<SyntaxNode>();
            memberNodes.AddRange(BuildMethods());
            memberNodes.AddRange(BuildProperties());

            return memberNodes;
        }

        private IEnumerable<SyntaxNode> BuildMethods()
        {
            List<SyntaxNode> methods = new List<SyntaxNode>();
            foreach (Method methodInfo in _class.Methods)
            {
                methods.AddRange(new MethodBuilder(Syntax, methodInfo).Build());
            }
            return methods;
        }

        private IEnumerable<SyntaxNode> BuildProperties()
        {
            foreach (Property propertyInfo in _class.Properties)
            {
                yield return new PropertyBuilder(Syntax, propertyInfo).Build();
            }
        }
    }
}