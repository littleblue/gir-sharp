using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Formatting;
using Microsoft.CodeAnalysis.Editing;

using System.Text;
using System.IO;
using System;

namespace Gir.Generation
{
    using Gir.Analysis;
    class RepositoryBuilder
    {
        private string workingDir;

        public RepositoryBuilder(string directory)
        {
            workingDir = directory;
        }

        public void Build(Repository repositoryInfo)
        {
            // TODO: Generation Options, Namespace tracking
            if (repositoryInfo.Namespace.Name == "GLib" ||
                repositoryInfo.Namespace.Name == "GObject")
                return;

            new NamespaceBuilder(repositoryInfo.Namespace, workingDir).Build();
        }
    }
}