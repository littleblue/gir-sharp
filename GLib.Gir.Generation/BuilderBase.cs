using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Formatting;
using Microsoft.CodeAnalysis.Editing;
using Microsoft.CSharp;

using System.Collections.Generic;

namespace Gir.Generation
{
    using Gir.Marshal;
    using Gir.Analysis;

    class BuilderBase
    {
        protected SyntaxGenerator Syntax { get; set; }

        public BuilderBase(SyntaxGenerator gen) =>
            Syntax = gen;

        public SyntaxNode MarshalAsToken(string girType)
        {
            Marshaller marshaller = Marshaller.Instance;
            return SyntaxFactory.ParseTypeName(marshaller.Marshal(girType));
        }
    }

    static class BuilderHelperMethods
    {
        public static SyntaxNode WithDocumentation(this SyntaxNode node, Documentation docs)
        {
            // Follow this:
            // https://stackoverflow.com/questions/30695752/how-do-i-add-an-xml-doc-comment-to-a-classdeclarationsyntax-in-roslyn#30696186

            return node.WithLeadingTrivia(
                SyntaxFactory.TriviaList(
                    SyntaxFactory.Trivia(
                        SyntaxFactory.DocumentationComment(/* TODO HERE */)
                    )
                )
            );
        }
    }
}