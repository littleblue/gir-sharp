using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Formatting;
using Microsoft.CodeAnalysis.Editing;
using Microsoft.CSharp;

using System;
using System.Collections.Generic;

namespace Gir.Generation
{
    using Gir;
    using Gir.Analysis;
    using Gir.Marshal;

    class PropertyBuilder : BuilderBase
    {
        private Property _property;
        public PropertyBuilder(SyntaxGenerator gen, Property propertyInfo) : base(gen)
        {
            _property = propertyInfo;
        }

        public SyntaxNode Build()
        {
            // Determines whether the property is read/write only
            // We assume that the properties are readable/writable by default
            bool isNotWritable = !_property.Writable;
            bool isNotReadable = !_property.Readable;

            // SyntaxNode tree representing a property
            SyntaxNode propertyDecl = Syntax.PropertyDeclaration(
                name: Utils.PropertyToPascalCase(_property.Name),
                type: MarshalAsToken(_property.Type.Name),
                getAccessorStatements: BuildGetAccessor(),
                setAccessorStatements: null
            );

            // Return with attributes
            return Syntax.AddAttributes(
                declaration: propertyDecl,
                attributes: BuildPropertyAttribute()
            );
        }

        private SyntaxNode BuildPropertyAttribute()
        {
            return Syntax.Attribute(
                Syntax.IdentifierName("GLib.Property"),
                new[] { Syntax.AttributeArgument(Syntax.LiteralExpression(_property.Name)) }
            );
        }

        private IEnumerable<SyntaxNode> BuildGetAccessor()
        {
            // Return multiple statements
            return new[]
            {
                // GLib.Value value = GetProperty ("property-name");
                Syntax.LocalDeclarationStatement(
                    type: SyntaxFactory.ParseTypeName("GLib.Value"),
                    identifier: "value",
                    initializer: Syntax.InvocationExpression(
                        Syntax.IdentifierName("GetProperty"),
                        Syntax.Argument(
                            "name",
                            RefKind.None,
                            Syntax.LiteralExpression(_property.Name)
                        )
                    )
                ),

                // NativeType ret = (NativeType)value;
                Syntax.LocalDeclarationStatement(
                    type: MarshalAsToken(_property.Type.Name),
                    identifier: "ret",
                    initializer: Syntax.CastExpression(
                        MarshalAsToken(_property.Type.Name),
                        Syntax.IdentifierName("value")
                    )
                ),

                // value.Dispose ();
                Syntax.InvocationExpression(
                    Syntax.MemberAccessExpression(
                        Syntax.IdentifierName("value"),
                        Syntax.IdentifierName("Dispose")
                    )
                ),

                // return ret;
                Syntax.ReturnStatement(
                    Syntax.IdentifierName("ret")
                )
            };
        }
        
        private IEnumerable<SyntaxNode> BuildSetAccessor()
        {
            // Return multiple statements
            return new[]
            {
                // GLib.Value value = GetProperty ("property-name");
                Syntax.LocalDeclarationStatement(
                    type: SyntaxFactory.ParseTypeName("GLib.Value"),
                    identifier: "value",
                    initializer: Syntax.InvocationExpression(
                        Syntax.IdentifierName("GetProperty"),
                        Syntax.Argument(
                            "name",
                            RefKind.None,
                            Syntax.LiteralExpression(_property.Name)
                        )
                    )
                ),

                // NativeType ret = (NativeType)value;
                Syntax.LocalDeclarationStatement(
                    type: MarshalAsToken(_property.Type.Name),
                    identifier: "ret",
                    initializer: Syntax.CastExpression(
                        MarshalAsToken(_property.Type.Name),
                        Syntax.IdentifierName("value")
                    )
                ),

                // value.Dispose ();
                Syntax.MemberAccessExpression(
                    Syntax.IdentifierName("value"),
                    Syntax.IdentifierName("Dispose")
                ),

                // return ret;
                Syntax.ReturnStatement(
                    Syntax.IdentifierName("ret")
                )
            };
        }
    }
}