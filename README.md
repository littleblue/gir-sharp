# GirSharp
A GObject Introspection based codegen tool for C# built from the ground up.
It is designed to be as compatible as reasonably possible with the existing
Gtk# bindings. Please note this is in no way suitable for production, although
that is certainly the aim. The bindings are largely incomplete and as of
writing do not compile due to missing pieces.

This is a part time student project, and I am slowly chipping away at
implementing the entire gobject-introspection spec. The primary consumer
for this codegen tool will be a not-yet-started rewrite of my Scrivener-style
writing program [Bluedit](https://gitlab.com/littleblue/bluedit). The aim is for
the bindings to be comparable to other language bindings for Python/Go/etc, in
that subclassing is supported however it is not fully integrated in the GNOME
stack as is Rust and Vala (cannot be exposed to C).

## Why C#?
There's no particular reason I chose C#, other than that I liked the syntax of
Vala but wanted something more mainstream/easier to debug. Having had some C#
experience and with Microsoft's release of .NET as open source, I decided it
could be a valuable learning experience trying to make bindings. Plus, if
these are successful, it could open the door to more developer support for Gtk,
which is always good :)

## Architecture
As of now, the codegen is stuctured into a few folders of note:
 - GLib contains the directly imported source code from gtk#. This has not been
 modified and it is unlikely to be, given the aim is compatibility.
 - GLib.Common contains interop code also taken from gtk#
 - GLib.Gir contains the actual codegen tool I have written. This is the
 'consumer code' and the entry point to the program, so start here.
 - GLib.Gir.Analysis contains models for the various gobject elements and types.
 These are populated by the parser and nested inside each other, which could
 potentially allow for some form of code analysis later on. Presently, this is
 fed into the builder classes which then emit code.
 - GLib.Gir.Generation contain said builder classes. These take <Name>Info
 structs from Gir.Analysis and use them to call into Roslyn (the C# compiler
 platform) to then generate code.
 
This seems somewhat more complicated than GapiCodegen, which is the original
parser written for Gtk# but was done to separate the parsing and generation to
avoid another situation like Gapi -> GObject Introspection. Hopefully this
architecture will hold up in the future, fingers crossed!

## Contributing
Please. I don't know what I'm doing.