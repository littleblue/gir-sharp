﻿using System.Xml.Serialization;

namespace Gir.Analysis
{
	public partial class Implements
	{
		[XmlAttribute ("name")]
		public string Name;
	}
}
