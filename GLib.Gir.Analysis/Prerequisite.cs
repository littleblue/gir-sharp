﻿using System.Xml.Serialization;

namespace Gir.Analysis
{
	public partial class Prerequisite
	{
		[XmlAttribute ("name")]
		public string Name;
	}
}