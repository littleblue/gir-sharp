﻿using System;
using System.Xml.Serialization;

namespace Gir.Analysis
{
	public partial class Documentation
	{
		[XmlText]
		public string Text;
	}
}
