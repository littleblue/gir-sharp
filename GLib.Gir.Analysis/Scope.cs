﻿using System.Xml.Serialization;

namespace Gir.Analysis
{
	public enum Scope
	{
		[XmlEnum ("call")]
		Call,
		[XmlEnum ("async")]
		Async,
		[XmlEnum ("notified")]
		Notified
	}
}

