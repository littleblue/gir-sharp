﻿using System.Xml.Serialization;

namespace Gir.Analysis
{
	public partial class Package
	{
		[XmlAttribute ("name")]
		public string Name { get; set; }
	}
}