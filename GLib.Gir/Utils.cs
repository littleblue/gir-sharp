using System.Globalization;

namespace Gir
{
    public static class Utils
    {
        public static string SnakeToPascalCase(string target)
        {
            target = target.Replace("_", " ");
            TextInfo info = CultureInfo.CurrentCulture.TextInfo;
            target = info.ToTitleCase(target).Replace(" ", string.Empty);
            return target;
        }

        public static string PropertyToPascalCase(string target)
        {
            target = target.Replace("-", " ");
            TextInfo info = CultureInfo.CurrentCulture.TextInfo;
            target = info.ToTitleCase(target).Replace(" ", string.Empty);
            return target;
        }
    }
}