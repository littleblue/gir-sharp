﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;

using Gir;
using Gir.Analysis;
using Gir.Generation;
class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Gir# 3 - The GObject Introspection Codegen Tool");

        // TODO: Tidy
        Repository genRepo;
        IEnumerable<Repository> repos = Parser.Parse("input/Atk-1.0.gir", "C:\\gir", out genRepo);
        RepositoryBuilder builder = new RepositoryBuilder("build");
        foreach (Repository repo in repos)
            builder.Build(repo);
        Console.WriteLine("Done!");
    }
}