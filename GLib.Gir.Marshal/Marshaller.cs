using System.Collections.Generic;

namespace Gir.Marshal
{
    public class Marshaller
    {
        private static Marshaller _marshaller;
        public static Marshaller Instance {
            get {
                if (_marshaller == null)
                    _marshaller = new Marshaller();
                return _marshaller;
            }
        }

        private Dictionary<string, string> typeDict;
        public Marshaller()
        {
            typeDict = new Dictionary<string, string>();
            AddType("none", "void");
            AddType("utf8", "string");
            AddType("gboolean", "bool");
            AddType("gint", "int");
            AddType("guint", "uint");

            AddType("GObject.Object", "GLib.Object");
            AddType("GType", "GLib.GType");
            AddType("GObject.ValueArray", "GLib.ValueArray");
        }

        public void AddType(string girType, string csType)
        {
            typeDict.Add(girType, csType);
        }

        public string Marshal(string girType)
        {
            string csType;
            if (typeDict.TryGetValue(girType, out csType))
                return csType;
            else
                return girType;
        }
    }
}